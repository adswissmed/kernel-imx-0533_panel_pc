# kernel-imx-0533_panel_pc

Private copy of [kernel-imx](https://github.com/elettronicagf/kernel-imx)
to use in [AD Swiss Med](www.adswissmed.ch) projects.

Since the upstream company, [Elettronica GF](https://www.elettronicagf.it/), keeps
pushing changes that breaks backward compatibility, we keep a private copy with our patches
to use in projects with [buildroot](https://buildroot.org/).
